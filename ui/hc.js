function holaWrite (message, callback) {
  var xhr = new XMLHttpRequest()
  var url = '/fn/ReadWrite/holaWrite'
  xhr.open('POST', url, true)
  xhr.setRequestHeader('Content-type', 'application/json')
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      callback(xhr.responseText)
    }
  }
  xhr.send(message)
}

function holaRead (hash, callback) {
  var xhr = new XMLHttpRequest()
  var url = '/fn/ReadWrite/holaRead'
  xhr.open('POST', url, true)
  xhr.setRequestHeader('Content-type', 'application/json')
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      callback(xhr.responseText)
    }
  }
  xhr.send(hash)
}

